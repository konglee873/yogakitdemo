//
//  YogaDemoViewController.m
//  YogaKitDemo
//
//  Created by kong on 2019/9/22.
//  Copyright © 2019 kong. All rights reserved.
//

#import "YogaDemoViewController.h"
#import "YogaDemoView.h"
#import <YogaKit/UIView+Yoga.h>
#import <YYModel/NSObject+YYModel.h>
#import "YogaDemoModel.h"
#import <Masonry/Masonry.h>
#import "CustomStackView.h"

@interface YogaDemoViewController ()

@property (nonatomic, strong) YogaDemoView *yogaView;

@property (nonatomic, copy) NSArray *dataArray;

@property (nonatomic, strong) CustomStackView *sview;

@end

@implementation YogaDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupUI];
}

- (void)setupUI {
//    [self.view configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
//        layout.isEnabled = YES;
//        layout.width = YGPointValue(self.view.bounds.size.width);
//        layout.height = YGPointValue(self.view.bounds.size.height);
////        layout.alignItems = YGAlignCenter;
////        layout.justifyContent = YGJustifyCenter;
//    }];
    
//    self.yogaView = [[YogaDemoView alloc] initWithFrame:CGRectZero];
//    [self.view addSubview:self.yogaView];
//    [self.yogaView mas_makeConstraints:^(MASConstraintMaker *make) {
//        if (@available(iOS 11, *)) {
//            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
//            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
//            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
//            make.bottom.equalTo(self.view.mas_bottom);
//        } else {
//            make.edges.equalTo(self.view);
//        }
//    }];
    
    self.sview = [[CustomStackView alloc] initWithFrame:CGRectZero];
    self.sview.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.sview];
    [self.sview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_top).offset(150);
        make.centerX.equalTo(self.view);
    }];
    
    
//    [self.view.yoga applyLayoutPreservingOrigin:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self bindData];
    });
}

- (void)bindData {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"yogademo" ofType:@".json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    if ([dic[@"data"] isKindOfClass:NSArray.class]) {
        NSArray *arr = (NSArray *)dic[@"data"];
        NSMutableArray *mArr = [NSMutableArray array];
        for (NSDictionary *dic in arr) {
            YogaDemoModel *model = [YogaDemoModel yy_modelWithDictionary:dic];
            [mArr addObject:model];
        }
        self.dataArray = [mArr copy];
    }
    
    if (self.dataArray.count <= 0) {
        return;
    }
    
    [self.sview bindWith:self.dataArray[0]];
    
//    [self.yogaView bind:self.dataArray[0]];
//    [self.view.yoga applyLayoutPreservingOrigin:NO];
}


@end
