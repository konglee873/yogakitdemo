//
//  YogaDemoView.h
//  YogaKitDemo
//
//  Created by kong on 2019/9/22.
//  Copyright © 2019 kong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YogaDemoModel;

NS_ASSUME_NONNULL_BEGIN

@interface YogaDemoView : UIView

- (void)bind:(nullable YogaDemoModel *)model;

@end

NS_ASSUME_NONNULL_END
