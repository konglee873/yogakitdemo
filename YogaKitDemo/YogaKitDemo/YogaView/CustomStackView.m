//
//  CustomStackView.m
//  YogaKitDemo
//
//  Created by kong on 2019/9/24.
//  Copyright © 2019 kong. All rights reserved.
//

#import "CustomStackView.h"
#import "YogaDemoModel.h"
#import <UserCollectionView/UIStackView+CustomSpacing.h>
#import <Masonry/Masonry.h>

@interface CustomStackView()
/// 性别
@property (nonatomic, strong) UIImageView *genderImgView;

/// 用户等级
@property (nonatomic, strong) UIImageView *levelImgView;

/// 贵族
@property (nonatomic, strong) UIImageView *nobityImgView;

/// 主播等级
@property (nonatomic, strong) UIView *anchorLevelView;

/// 真爱团
@property (nonatomic, strong) UIView *fanClubView;

@property (nonatomic, assign) CGFloat totalWidth;

@end

@implementation CustomStackView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        [self configSetting];
        [self setupUI];
    }
    return self;
}

/*
 UIStackViewDistributionFill = 0,
 
 UIStackViewDistributionFillEqually,
 
 UIStackViewDistributionFillProportionally,
 
 UIStackViewDistributionEqualSpacing,
 UIStackViewDistributionEqualCentering,
 */

- (void)configSetting {
    self.axis = UILayoutConstraintAxisHorizontal;
    self.alignment = UIStackViewAlignmentCenter;
    self.distribution = UIStackViewDistributionFillProportionally;
    self.spacing = 5.0;
//    self.layoutMarginsRelativeArrangement = YES;
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(159, 15);
}

- (void)setupUI {
    _genderImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    _genderImgView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.1];
    _levelImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 15)];
    _levelImgView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
    _nobityImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 15)];
    _nobityImgView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
    _anchorLevelView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 15)];
    _anchorLevelView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8];
    _fanClubView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 38, 15)];
    _fanClubView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:1];
    NSArray <UIView *> *views = @[_genderImgView, _levelImgView, _nobityImgView, _anchorLevelView, _fanClubView];
    for (UIView *v in views) {
        [self addArrangedSubview:v];
    }
    
    [_genderImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    [_levelImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@30);
        make.height.equalTo(@15);
    }];
    
    [_nobityImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@26);
        make.height.equalTo(@15);
    }];
    
    [_anchorLevelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@30);
        make.height.equalTo(@15);
    }];
    
    [_fanClubView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@38);
        make.height.equalTo(@15);
    }];
    
    _totalWidth = 15 + 30 + 26 + 30 + 38 + 5 * 4;
}

- (void)bindWith:(nonnull YogaDemoModel *)info {
    [_anchorLevelView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@0);
        make.height.equalTo(@15);
    }];
    
    _totalWidth = _totalWidth - 30;
    [self setNeedsLayout];
}

@end

