//
//  CustomStackView.h
//  YogaKitDemo
//
//  Created by kong on 2019/9/24.
//  Copyright © 2019 kong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YogaDemoModel;

NS_ASSUME_NONNULL_BEGIN

@interface CustomStackView : UIStackView

- (void)bindWith:(nonnull YogaDemoModel *)info;

@end

NS_ASSUME_NONNULL_END
