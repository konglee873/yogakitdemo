//
//  YogaDemoView.m
//  YogaKitDemo
//
//  Created by kong on 2019/9/22.
//  Copyright © 2019 kong. All rights reserved.
//

#import "YogaDemoView.h"
#import <YogaKit/UIView+Yoga.h>
#import "YogaDemoModel.h"

@interface YogaDemoView()

@property (nonatomic, strong) UIView *view1;

@property (nonatomic, strong) UIView *view2;

@property (nonatomic, strong) UIView *view3;

@property (nonatomic, strong) UIView *view4;

@property (nonatomic, strong) UIView *view5;

@property (nonatomic, strong) UILabel *label6;

@end

@implementation YogaDemoView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    _view1 = [UIView new];
    _view2 = [UIView new];
//    _view3 = [UIView new];
//    _view4 = [UIView new];
//    _view5 = [UIView new];
//    _label6 = [UILabel new];
//    NSArray <UIView *> *views = @[_view1, _view2, _view3, _view4, _view5, _label6];
//    [views enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        obj.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:(idx + 1) / 6.0];
//        [self addSubview:obj];
//    }];
    
    self.backgroundColor = [UIColor lightGrayColor];
    [self configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
        layout.isEnabled = YES;
        layout.flexDirection = YGFlexDirectionRow;
        layout.width = YGPointValue(300);
        layout.height = YGPointValue(80);
//        layout.top = YGPointValue(100);
//        layout.justifyContent = YGJustifyCenter;
//        layout.alignItems = YGAlignCenter;
//        layout.marginTop = YGPointValue(100);
//        layout.left = YGPointValue(20);
    }];
    
    _view1.backgroundColor = [UIColor redColor];
    [_view1 configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
        layout.marginTop = YGPointValue(10);
        layout.marginBottom = YGPointValue(20);
        layout.isEnabled = YES;
        layout.width = YGPointValue(80);
        layout.marginRight = YGPointValue(10);
        layout.marginLeft = YGPointValue(10);
    }];
    [self addSubview:_view1];
    
    _view2.backgroundColor = [UIColor yellowColor];
    [_view2 configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
        layout.isEnabled = true;
        layout.width = YGPointValue(80);
        layout.flexGrow = 1;
        layout.marginBottom = YGPointValue(10);
        layout.marginTop = YGPointValue(10);
        layout.marginRight = YGPointValue(20);
    }];
    [self addSubview:_view2];
    
    
    
    _label6 = [UILabel new];
    _label6.numberOfLines = 3;
    _label6.font = [UIFont systemFontOfSize:10];
    _label6.textColor = [UIColor cyanColor];
    _label6.text = @"1975年初农民子弟孙少平到原西县高中读书，他贫困，自卑；后对处境相同的地主家庭出身的郝红梅产生情愫，在被同班同学侯玉英发现并当众说破后，与郝红梅关系渐变恶劣，后来郝红梅却与家境优越的顾养民恋爱。少平高中毕业，回到家乡做了一名教师。但他并没有消沉，他与县革委副主任田福军女儿田晓霞建立了友情，在晓霞帮助下关注着外面的世界。少平的哥哥少安一直在家劳动，与村支书田福堂的女儿——县城教师田润叶青梅竹马。少安和润叶互有爱慕之心，却遭到田福堂反对。经过痛苦的煎熬，少安到山西与勤劳善良的秀莲相亲并结了婚，润叶也只得含泪与父亲介绍的一直对她有爱慕之情的李向前结婚。这时农村生活混乱，又遇上了旱灾，田福堂为了加强自己的威信，组织偷挖河坝与上游抢水，不料竟出了人命。为了“农业学大寨”，他好大喜功炸山修田叫人搬家又弄得天怒人怨。生活的航道已改变地步。";
    
//    [self addSubview:_label6];
    
    [self.yoga applyLayoutPreservingOrigin:YES];


}


- (void)bind:(nullable YogaDemoModel *)model {
    [_view1 configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
        layout.marginTop = YGPointValue(10);
        layout.marginBottom = YGPointValue(20);
        layout.isEnabled = YES;
        layout.width = YGPointValue(model.v1width);
        layout.marginRight = YGPointValue(10);
        layout.marginLeft = YGPointValue(10);
    }];
    [self.yoga applyLayoutPreservingOrigin:NO];
}


@end
