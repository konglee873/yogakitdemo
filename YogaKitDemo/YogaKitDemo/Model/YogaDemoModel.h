//
//  YogaDemoModel.h
//  YogaKitDemo
//
//  Created by kong on 2019/9/22.
//  Copyright © 2019 kong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YogaDemoModel : NSObject
/*
 v1width" : 10,
 "v2width" : 20,
 "v3width" : 30,
 "v4width" : 3,
 "v5width" : 50,
 "v6width
 */

@property (nonatomic, assign) NSInteger v1width;

@property (nonatomic, assign) NSInteger v2width;

@property (nonatomic, assign) NSInteger v3width;

@property (nonatomic, assign) NSInteger v4width;

@property (nonatomic, assign) NSInteger v5width;

@property (nonatomic, assign) NSInteger v6width;

@end

NS_ASSUME_NONNULL_END
